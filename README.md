[![coverage](https://gitlab.com/hs-karlsruhe/ws2020/rofl1017/webservice/badges/master/coverage.svg)](https://hs-karlsruhe.gitlab.io/hs-karlsruhe/ws2020/rofl1017/webservice/test/)

[![markdownlint](https://hs-karlsruhe.gitlab.io/ws2020/rofl1017/webservice/badges/markdownlint.svg)](https://gitlab.com/hs-karlsruhe.de/ws2020/rofl1017/webservice/commits/master)

[![yamllint](https://hs-karlsruhe.gitlab.io/ws2020/rofl1017/webservice/badges/yamllint.svg)](https://gitlab.com/hs-karlsruhe/ws2020/rofl1017/webservice/commits/master)

[![pylint](https://hs-karlsruhe.gitlab.io/ws2020/rofl1017/webservice/badges/pylint.svg)](https://hs-karlsruhe.gitlab.io/ws2020/rofl1017/webservice/lint/)
